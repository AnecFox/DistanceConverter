package com.anec.distance;

public class Kilometer {

    private final static int KILOMETER_TO_CENTIMETERS = 100000;

    public static double kilometersToCentimeters(double value) {
        return value * KILOMETER_TO_CENTIMETERS;
    }

    public static double centimetersToKilometers(double value) {
        return value / KILOMETER_TO_CENTIMETERS;
    }
}
