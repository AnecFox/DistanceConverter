package com.anec.distance;

public class Millimeter {

    private final static int CENTIMETER_TO_MILLIMETERS = 10;

    public static double millimetersToCentimeters(double value) {
        return value / CENTIMETER_TO_MILLIMETERS;
    }

    public static double centimetersToMillimeters(double value) {
        return value * CENTIMETER_TO_MILLIMETERS;
    }
}
