package com.anec;

import com.anec.distance.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class MainWindow extends JFrame {

    private final static String VERSION = "0.1.3";

    private final JTextField textFieldValue = new JTextField();
    private final JTextField textFieldResult = new JTextField();

    private final JButton buttonCalculate = new JButton("Рассчитать");
    private final JButton buttonAbout = new JButton("О программе");

    public MainWindow() {
        initialize();
    }

    private void initialize() {
        setTitle("Distance Converter");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(512, 140);
        setMinimumSize(new Dimension(256, 128));
        setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getClassLoader()
                .getResource("com/anec/icon/icon.png")));

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(3, 2, 5, 5));

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        textFieldValue.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    buttonCalculate.doClick();
                }
            }
        });
        container.add(textFieldValue);

        textFieldResult.setEditable(false);
        container.add(textFieldResult);

        String[] units = new String[]{
                "Миллиметры", "Сантиметры", "Дюймы", "Дециметры", "Метры", "Километры"
        };

        JComboBox<String> comboBoxFirstUnit = new JComboBox<>(units);
        comboBoxFirstUnit.setSelectedItem("Метры");
        container.add(comboBoxFirstUnit);

        JComboBox<String> comboBoxSecondUnit = new JComboBox<>(units);
        comboBoxSecondUnit.setSelectedItem("Сантиметры");
        container.add(comboBoxSecondUnit);

        buttonCalculate.addActionListener(e -> {
            double value;

            try {
                value = Double.parseDouble(textFieldValue.getText().contains(",") ?
                        textFieldValue.getText().trim().replace(',', '.') :
                        textFieldValue.getText().trim());
            } catch (NumberFormatException ex) {
                if (textFieldValue.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Значение не введено",
                            this.getTitle(), JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Значение неправильно введено",
                            this.getTitle(), JOptionPane.ERROR_MESSAGE);
                }
                return;
            }

            double result = 0;

            if ("Миллиметры".equals(comboBoxFirstUnit.getSelectedItem())) {
                result = Millimeter.millimetersToCentimeters(value);
            } else if ("Дюймы".equals(comboBoxFirstUnit.getSelectedItem())) {
                result = Inch.inchesToCentimeters(value);
            } else if ("Дециметры".equals(comboBoxFirstUnit.getSelectedItem())) {
                result = Decimeter.decimetersToCentimeters(value);
            } else if ("Метры".equals(comboBoxFirstUnit.getSelectedItem())) {
                result = Meter.metersToCentimeters(value);
            } else if ("Километры".equals(comboBoxFirstUnit.getSelectedItem())) {
                result = Kilometer.kilometersToCentimeters(value);
            }

            if ("Миллиметры".equals(comboBoxSecondUnit.getSelectedItem())) {
                result = Millimeter.centimetersToMillimeters(result);
            } else if ("Дюймы".equals(comboBoxSecondUnit.getSelectedItem())) {
                result = Inch.centimetersToInches(result);
            } else if ("Дециметры".equals(comboBoxSecondUnit.getSelectedItem())) {
                result = Decimeter.centimetersToDecimeters(result);
            } else if ("Метры".equals(comboBoxSecondUnit.getSelectedItem())) {
                result = Meter.centimetersToMeters(result);
            } else if ("Километры".equals(comboBoxSecondUnit.getSelectedItem())) {
                result = Kilometer.centimetersToKilometers(result);
            }

            if ("Сантиметры".equals(comboBoxFirstUnit.getSelectedItem())) {
                if ("Миллиметры".equals(comboBoxSecondUnit.getSelectedItem())) {
                    result = Millimeter.centimetersToMillimeters(value);
                } else if ("Сантиметры".equals(comboBoxSecondUnit.getSelectedItem())) {
                    result = value;
                } else if ("Дюймы".equals(comboBoxSecondUnit.getSelectedItem())) {
                    result = Inch.centimetersToInches(value);
                } else if ("Дециметры".equals(comboBoxSecondUnit.getSelectedItem())) {
                    result = Decimeter.centimetersToDecimeters(value);
                } else if ("Метры".equals(comboBoxSecondUnit.getSelectedItem())) {
                    result = Meter.centimetersToMeters(value);
                } else if ("Километры".equals(comboBoxSecondUnit.getSelectedItem())) {
                    result = Kilometer.centimetersToKilometers(value);
                }
            }

            String stringResult = textFieldValue.getText().contains(".") ?
                    String.valueOf(result) : String.valueOf(result).replace('.', ',');

            textFieldResult.setText(stringResult.endsWith(",0") || stringResult.endsWith(".0") ?
                    stringResult.replace((stringResult.contains(".") ? ".0" : ",0"), "") : stringResult);
        });
        container.add(buttonCalculate);

        buttonAbout.addActionListener(e -> JOptionPane.showMessageDialog(null,
                "Эта программа предназначена для рассчёта расстояния.\n\n" +
                        "                                        Версия: " + VERSION + "\n\n" +
                        "                                       Created by Anec", "О программе",
                JOptionPane.PLAIN_MESSAGE));
        container.add(buttonAbout);

        for (Component c : this.getComponents()) {
            SwingUtilities.updateComponentTreeUI(c);
        }
    }
}
